-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 31-Out-2018 às 19:50
-- Versão do servidor: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `database_logs`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(100) NOT NULL,
  `Time_spent` varchar(100) NOT NULL,
  `Date` datetime NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `logs`
--

INSERT INTO `logs` (`Id`, `Description`, `Time_spent`, `Date`) VALUES
(1, 'Did Nothing', '1h', '2018-10-31 19:23:54'),
(2, 'Sleep', '1h', '2018-10-31 19:26:22'),
(3, 'Play ', '1h 10m', '2018-10-31 19:30:38'),
(4, 'Eat', '1h 20m', '2018-10-31 19:31:24'),
(5, 'Went do Shop', '1h 20m', '2018-10-31 19:34:06'),
(6, 'Went do Run', '1h 20m', '2018-10-31 19:34:34'),
(7, 'Went to play football', '2h 30m', '2018-10-31 19:35:18'),
(8, 'Went to play basketball', '2h 30m', '2018-10-31 19:36:33'),
(9, 'More Sleep', '2h 10m', '2018-10-31 19:39:19'),
(10, 'Listen Music', '30m', '2018-10-31 19:48:44');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
