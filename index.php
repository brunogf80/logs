<?php
require_once 'classes/Logs.class.php';
?>

<!DOCTYPE HTML>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>My Tyme log</title>
	<link href="css/styles.css" rel="stylesheet" type="text/css" media="all">
	<script type="text/javascript" src="js/jquery.js"></script>


</head>
<body>
	<div class="container">
		<h2>My Time Log</h2>
		<hr>
		<div id="logsList">
			<!--logs list-->
		</div>

		<div id="logsForm">
			<!--form-->
			<form class="form">
				<label>Description</label>
				<input type="text" placeholder="Description" maxlength="100" name="description" id="description">
				<div class="clear"></div>
				<label>Time Spent</label>
				<input type="text" placeholder="Time Spent" name="time_spent" id="time_spent">
				
				
				<button type="button" class="btn">Submit</button>
				<div class="clear"></div>
				<div class="message"></div>
			</form>

		</div>
	</div>

</body>

</html>
<script>
$( document ).ready(function() {

	$( "#logsList" ).load( "list.php" );
	//$('#table_today').DataTable();
  $('.btn').click(function() {
	$.ajax({
		url : 'insert.php',
		cache: false,
		type: 'post',
		dataType: 'json',
		data: { description: $('#description').val(), time_spent: $('#time_spent').val() }
	})  .done(function( json ) {
		$('.message').html(json.message);
		$('#description').attr('value','');
		$('#time_spent').attr('value','');
		$( "#logsList" ).load( "list.php" );
	});
  });

});
</script>