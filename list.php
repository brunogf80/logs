<?php
require_once 'classes/Logs.class.php';
$logs = new Logs();
$today = date('Y-m-d');
$list = $logs->querySelect();



?>
<link href="css/jquery.dataTables.css" rel="stylesheet" type="text/css" media="all">
<link href="css/tables.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/styles.css" rel="stylesheet" type="text/css" media="all">
<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>

<?php
$param_date = null;
for($counter=0;$counter<=count($list)-1;$counter++){
	$exp_date = explode(" ", $list[$counter]['Date']);
	

if($param_date!=$exp_date[0]){
	$param_date = $exp_date[0];
?>

<div class="clear"></div>
<div class="clear"></div>
<div class="clear"></div>
<table data-order='[[ 2, "desc" ]]' data-page-length='5' id= "table_<?php echo $counter?>" class="table table-bordered display nowrap dataTable dtr-inline collapsed" style="width: 640px; margin-top: 100px" >
	
	<thead>

		<tr>
			<th colspan="3" align="left"><?php echo $exp_date[0] == $today ? "Today": $exp_date[0] ?></th>
			
		</tr>
	
		<tr>
			<th align="left">Description</th>
			<th align="left">Time Spent</th>
			<th align="left">Date</th>
		</tr>


	</thead>
	<tbody>
<?php
}
if($param_date==$exp_date[0]){
?>
		<tr>
			<td><?php echo $list[$counter]['Description'] ?></td>
			<td><?php echo $list[$counter]['Time_spent']?></td>
			<td><?php echo $list[$counter]['Date']?></td>
		</tr>
<?php
}
else
{
?>
	</tbody>
</table>

	<?php
	}
	?>
<?php
	
}
?>
<input type="hidden" value="<?php echo $counter?>" name="counter" id="counter">


<script>
$(document).ready( function () {
	var i;
	for (i = 0; i < $('#counter').val(); i++) { 
   		console.log($('#table_'+i));
   		$('#table_'+i).DataTable( {
    		searching: true,
    	 	lengthChange: false
		} );
		$('#table_'+i+"_wrapper").css('margin-bottom','30px');

   	}
} );
</script>

