<?php
require_once 'classes/Logs.class.php';
$logs = new Logs();

$logs->__set('description',$_POST['description']);
$logs->__set('time_spent',$_POST['time_spent']);
$logs->__set('date', date('Y-m-d H:i:s'));

$insert =  $logs->queryInsert();
$json['message'] = $insert == true ? 'Log inserted successfully':'Failed to insert log. Please try again';
echo json_encode($json);
// $select = $logs->querySelect();
// var_dump($select);
?>