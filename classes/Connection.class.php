<?php

Class Connection{
	private $user;
	private $password;	
	private $database;
	private $server;
	private static $pdo;

	public function __construct()
	{
		$this->server  = 'localhost';
		$this->database = 'database_logs';
		$this->user = 'root';
		$this->password = '';
	}

	public function connect()
	{
		try{
			$flags = array(
				PDO::ATTR_PERSISTENT => false,
				PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true,
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
				PDO::MYSQL_ATTR_INIT_COMMAND =>'SET NAMES utf8'
			);
			
			if(is_null(self::$pdo)){
				self::$pdo = new PDO("mysql:host=".$this->server.";port=3306;dbname=".$this->database,$this->user,$this->password,$flags);
			}
			
			return self::$pdo;
		}catch(PDOException $ex){
			echo $ex->getMessage();
		}
	}
}

?>