<?php

require_once 'Connection.class.php';
require_once 'Functions.class.php';

class Logs{
	private $conn;
	private $objfunc;
	private $description;
	private $time_spent;
	private $date;

	public function __construct()
	{
		$this->con = new Connection();
		$this->objfunc = new Functions();
	}

	public function __set($attribute,$value)
	{
		$this->$attribute = $value;
	}

	public function __get($attribute){
        return $this->$attribute;
    }

    public function querySelect($date = null){
        try{
        
           	$cst = $this->con->connect()->prepare("SELECT `Id`, `Description`, `Time_spent`, `Date` FROM `logs` group by Date order by Id desc;");
            $cst->execute();
            //echo $cst->queryString;
            return $cst->fetchAll();
        } catch (PDOException $ex) {
            return 'error '.$ex->getMessage();
        }
    }

    public function queryInsert(){
        try{
        	$cst = $this->con->connect()->prepare("INSERT INTO `logs` (`Description`, `Time_spent`, `Date`) VALUES (:description,:time_spent,:dt)");
           	$cst->bindParam(":description", $this->description, PDO::PARAM_STR);
            $cst->bindParam(":time_spent", $this->time_spent, PDO::PARAM_STR);
            $cst->bindParam(":dt", $this->date, PDO::PARAM_STR);
			return $cst->execute();
        
        } catch (PDOException $ex) {
            return 'error '.$ex->getMessage();
        }
    }


}

?>